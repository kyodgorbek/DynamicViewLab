package com.example.dynamicviewlab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

/**
 * Created by yodgorbek on 22.10.15.
 */
public class GridActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);
        int maxItems = getIntent().getExtras().getInt(Intent.EXTRA_UID);
        GridView gridView = (GridView) findViewById(R.id.gvItems);
        gridView.setAdapter(new ListGridAdapter(getLayoutInflater(),
                ListGridAdapter.AdapterMode.GRID, maxItems));
        gridView.setOnItemClickListener(parent, view, position, id) -> {
            Toast.LENGTH_LONG).show();

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
       // Inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.menu_grid, menu);
        return true;
    }

