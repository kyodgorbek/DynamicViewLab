package com.example.dynamicviewlab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.widget.ListView;

/**
 * Created by yodgorbek on 22.10.15.
 */
public class ListActivity extends ActionBarActivity  {

   private static final int MAX_LIST_ITEMS = 30;


    @Override
   protected void onCreate(Bundle savedInstanceState){
       super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_list);
       ListView listView = (ListView) findViewById(R.id.lvItems);
       listView.setAdapter(new ListGridAdapter(getLayoutInflater();,
               ListGridAdapter.AdapterMode.LIST, MAX_LIST_ITEMS);
       listView.setOnItemClickListener(parent, view, position, id) -> {
           Intent gridIntent = new Intent(view.getContext(), GridActivity.class);
           gridIntent.putExtra(Intent.EXTRA_UID,(int)Parent.getAdapter().getItem(position));
            ListActivity.this.startActivity(gridIntent);

        });

   }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {}
